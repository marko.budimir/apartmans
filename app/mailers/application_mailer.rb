class ApplicationMailer < ActionMailer::Base
  default from: 'marko.budimir10@gmail.com'
  layout 'mailer'
end
